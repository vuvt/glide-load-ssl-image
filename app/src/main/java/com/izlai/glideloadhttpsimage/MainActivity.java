package com.izlai.glideloadhttpsimage;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

/**
 * Create by thaivu at 15/5/2018
 */
public class MainActivity extends AppCompatActivity {

    ImageView img;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        img = (ImageView) findViewById(R.id.img);

        Glide.with(this)
                .load("https://gts.aolinhxanh.com/app/image/convert/DVH/image/HUB-images/hub-amnhac.png")
//                .apply(RequestOptions()
//                        .override(100, 100)
//                        .placeholder(R.drawable.ic_default_avatar)
//                        .error(R.drawable.ic_default_avatar))
                .into(img);
    }
}
